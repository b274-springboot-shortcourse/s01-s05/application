package com.example.zuitt.application.repositories;


//An interface contains behavior that class implements
// An interface marked @Respository contains method for database
// By extending CrudRepository, postRepository has inherited its pre-defined methods for creating, retrieving, updating and deleting

import com.example.zuitt.application.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {




}
