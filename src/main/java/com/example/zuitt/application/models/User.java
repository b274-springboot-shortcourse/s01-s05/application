package com.example.zuitt.application.models;
import javax.annotation.processing.Generated;
import javax.persistence.*;


@Entity
@Table(name="user")
public class User {

    @id

    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

//    Constructor
    public User(){}

//    Sub-constructor
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

//    getters
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password
    }

//    setters
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }

}
