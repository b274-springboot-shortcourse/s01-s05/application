package com.example.zuitt.application.models;
import javax.persistence.*; // retrieval and insertion of data

// Mark this java object as a representation of a DB table via @Entity
@Entity
// creation or designation of table name via @Table
@Table(name="posts")

public class Post {
//    Indicate that this property represents the primary key via @id
    @id
//    values for this property will be auto-incremented

    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

//    Default constructor, this is needed when retrieving posts
    public Post(){}

//   Sub constructor
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

//    getter for Title
    public String getTitle(){
        return title;
    }

//    setters for Title
    public void setTitle(String title){
        this.title = title;
    }

    //    getter for Content
    public String getContent(){
        return content;
    }

    //    setters for Content
    public void setContent(String content){
        this.content = content;
    }


}
