package com.example.zuitt.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//Annotations
@SpringBootApplication
@RestController // This tells Spring Boot that this is an end point that will be used in handling web pages
// @RestController gives authorization para gumamit ng mga end points
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// may hawak ng end point mismo
	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "world") String name) {
		return String.format("Hello %s!", name);
	}
		@GetMapping("/shape")
		public String shape(@RequestParam(value = "shape", defaultValue = "error") String shapeName, int numberOfSides) {
			return String.format("Your shape is " + shapeName + ". It has " + numberOfSides + " sides.");
	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "error") String name, int age){
		return String.format("Hello " + name + "! Your age is " + age + ".");
	}

//	@RequestParam = magmamanipulate ng data sa browser
//	String.format equivalent to sout
//	params with value = http://localhost:8080/hello?name=Jane
//	http://localhost:8080/hi?shape=triangle&numberOfSides=3
//	make sure to rerun if changes made in the code base

	}





